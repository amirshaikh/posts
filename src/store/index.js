import { createStore } from "vuex";
const state = {
  user: {
    id: null,
  },
  posts: [],
};

const getters = {
  posts: (state) => {
    let posts = state.posts.filter((post) => post.userId == state.user.id);
    return posts && posts.length ? posts : [];
  },
  user: (state) => {
    return state.user ? state.user : {};
  },
};

const actions = {
  // Login
  Login: ({ commit }, user) => {
    return new Promise((fulfill) => {
      commit("setUser", user);
      fulfill(true);
    });
  },
  // Logout
  Logout: ({ commit }) => {
    commit("logout");
  },
  // Posts
  Add: ({ commit }, post) => {
    commit("add", post);
  },
};

const mutations = {
  setUser: (state, user) => {
    state.user.id = user.userId;
  },
  logout: (state) => {
    state.user.id = null;
  },
  add: (state, post) => {
    state.posts.push(post);
  },
};

export const store = createStore({
  state,
  getters,
  actions,
  mutations,
});

export default store;
